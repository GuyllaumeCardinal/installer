#!/usr/bin/env bash

red='\033[0;31m'
green='\033[1;32m'
nc='\033[0m' # No Color

log_info() {
    message=$1
    echo -e "${green}--->${nc} ${message}"
}

# Check that we are running as root, required to install requirements.
if [ "$EUID" -ne 0 ]; then
    echo -e "${red}This script requires running as root.${nc}"
    echo -e "Run as root or with sudo."
    exit 1
fi

# Confirm that we have NodeJS v12+. If not, install it
if [ ! -x "$(command -v node)" ]; then
    log_info "NodeJS is missing on the system. Installing."

    log_info "Installing dependencies."
    sudo apt install -y libssl-dev
    curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -

    log_info "Installing NodeJS."
    sudo apt -y install nodejs
fi

# Confirm that we have unzip. If not, install it
if [ ! -x "$(command -v unzip)" ]; then
    log_info "unzip is missing on the system. Installing."

    log_info "Installing unzip."
    sudo apt install -y unzip
fi

log_info "All dependencies present on the system. Ready to install!"
