FoundryVTT Installer
---

We're not barbarians, we version control our install scripts.

## Before Installation
You will need the download link for FoundryVTT. It can be found in the Purchased Licenses page on
FoundryVTT. Click the link/chain icon to the right of the standard download link to obtain a
temporary download url for the software.

**You want the NodeJS version**.

## Installation
1. `sudo bin/install-dependency`
1. `bin/install "url_of_the_zip_file"`
    1. Do **not** forget the quotes around the URL.

The scripts themselves should also guide you. They are also, in theory idempotent.

## Starting And Stopping
It's a NodeJS script, so in theory you can simply run
`node app/foundryvtt/resources/app/main.js --dataPath=app/data`, but we provide utility
scripts to easily start the process in the background and stop it later on.

They are `bin/start` and `bin/stop`.

**IMPORTANT**: The `bin/stop` helper is naive, in that it checks the PID that `bin/start`
reported and sends a SIGKILL signal. **THIS COULD MEAN YOU KILL A DIFFERENT PROCESS THAN
FOUNDRYVTT**. Be very careful with this command.
